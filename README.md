# Create an EKS cluster from AWS Console

![EKS plan](images/plan.png)

### For further detail refer to the [handout](11-K8sonAWSHandout.pdf) and [AWS official documentation](https://docs.aws.amazon.com/eks/latest/userguide/what-is-eks.html).

## EKS Cluster Role

1.- Create a rol with `AmazonEKSWorkerNodePolicy` for EKS Cluster.

![eks-cluster-role](images/eks-cluster-role.png)

## VPC cloudformation template for public and private subnets

2.- Create a VPC for the cluster and worker nodes.

[Template from AWS documentation](https://s3.us-west-2.amazonaws.com/amazon-eks/cloudformation/2020-10-29/amazon-eks-vpc-private-subnets.yaml)

## Cluster creation official [docs](https://docs.aws.amazon.com/eks/latest/userguide/getting-started-eksctl.html)

3.- Connect to the cluster.

## Connecting to you cluster once it is created
### Get your API server endpoint to connect to by getting the arn of it and configuring kubectl client.

![API Server Endpoint](images/apiServerEndPoint.png)

`aws eks update-kubeconfig --name eks-cluster-test`

## Validate conection 

`$ kubectl cluster-info`

You should get connected to:

![EKS Cluster](images/eks-cluster-test.png)
![Validate connection](images/validateConection.png)

## Creating the worker nodes

#### Two ways to create them

### 1. Node Groups

#### Create a worker node rol to make kubelet running on those EC2 nodes able to make api calls to others aws services as well as other tasks

 1.1. Create a rol with `AmazonEKSWorkerNodePolicy`, `AmazonEC2ContainerRegistryReadOnly` and `AmazonEKS_CNI_Policy`

![eks-node-group-role](images/eks-node-group-rol.png)


1.2. Create worker node group with proper autoscaling according to your environment and app.


![eks-worker-nodes](images/eks-node-group-config.png)

I've created them in spot capacity type as they're cheaper.

![nodes-created](images/worker-nodes-created.png)

![validate-connection](images/validate-connection-worker-nodes.png)

K8s components that were installed  as we created the worker nodes are:

- kubelet
- kproxy
- containerd

So they're ready to deploy services to.

#### Configuring autoscaling for worker nodes

Based on [Offical Docs](https://docs.aws.amazon.com/eks/latest/userguide/autoscaling.html)

EKS has created an Auto Scaling Group

![EKS-WorkerNodes-ASG](images/eks-wn-asg.png)

But you need to configure k8s autoscaler in order to balance the pods inside the worker nodes optimizing the use of them. For that these has to be created as well:

- Custom policy to be attach to `eks-node-group-rol` [policy](eks-node-group-autoscale-policy.json) so it would be like:

![eks Node group rol](images/eks-ng-rol-autoscale.png)

- Deploy the autoscaler in the cluster:

    `$ kubectl apply -f https://raw.githubusercontent.com/kubernetes/autoscaler/master/cluster-autoscaler/cloudprovider/aws/examples/cluster-autoscaler-autodiscover.yaml`

validate deployment:
`$ kubectl get deployment -n kube-system`

![ValidateDeployment](images/validateAutoScalerDeployment.png)

Then we need to tweak the autoscaler deployment by changing:

`$ kubectl edit deployment -n kube-system cluster-autoscaler`

then modify it according to [autoscaler-deployment](deployment-edited.yaml)

Validate pods:
`$ kubectl get pods -n kube-system`

Check on which node is autoscaler running:
`$ kubectl get pod cluster-autoscaler-65d5bd6b96-bg6xl -n kube-system -o wide`

![podAutoscalerValidation](images/pod-autoscaler-validations.png)

To get the logs of the autoscaler:

`$ kubectl logs -n kube-system cluster-autoscaler-65d5bd6b96-bg6xl`

#### Testing the autoscaling by deploying 20 nginx replicas 

`$ kubectl apply -f kubectl apply -f nginx.yaml`

![Testing](images/testing-autoscaling.png)

![Testing2](images/nginx-test.png)

Then edit the deployment to increase replica count to 20 and see the new node created and autoscaler moving pods to it.

![Increased-Replicas](images/increased-nginx-replicas.png)
![WorkerNodesConfig](images/workerNodesConfig.png)
![NewWorkerNodes](images/newWorkerNodes.png)
![podDist](images/podDistributionOnNodes.png)

### 2. Fargate Profile

### Create a new role to allow Fargate to schedule pods in new VMs

- Role Type: `Trusted entity type` - `Service EKS`
- Use case: `EKS - Fargate pod`

![eks-fargate-role](images/eks-fargate-role.png)

### Create a Fargate profile

#### Make sure you use the private subnets for your pods.

![fargate-config1](images/fargate-config1.png)

![fargate-config2](images/fargate-config2.png)

#### By using the namespace label selector you can make use of the fargate profile for your pods.

![deployment-config-fargate](images/dev-fargate-profile.png)

#### Befor you can apply the the pod in fargate, thge namespace `dev` need to exists locally so:

`$ kubectl create ns dev`

![dev-ns-local](images/dev-ns-local.png)

#### And then 

`$ kubectl apply -f nginx-fargate.yaml`

![nginx-dep-fargate](images/nginx-dep-fargate.png)

# Create an EKS Cluster from eksctl tool

[Official Docs](https://eksctl.io/)

![eksctl](images/eksctl.png)

`eksctl` is a simple CLI tool for creating and managing clusters on EKS - Amazon's managed Kubernetes service for EC2. It is written in Go, uses CloudFormation, was created by [Weaveworks](https://www.weave.works/) and it welcomes contributions from the community. Create a basic cluster in minutes with just one command:

`$ eks create cluster`

But with the defaults what's been created is:

- exciting auto-generated name, e.g., fabulous-mushroom-1527688624
- two m5.large worker nodes—this instance type suits most common use-cases, and is good value for money
- use the official AWS EKS AMI
- us-west-2 region
- a dedicated VPC (check your quotas)

We can override these in 2 ways with something like:

    $ eksctl create cluster \
            --name demo-cluster \
            --version 1.23 \
            --region us-east-1 \
            --nodegroup-name demo-nodes \
            --nodetype t2.micro \
            --nodes 2 \
            --nodes-min 1 \
            --nodes-max 3

Or as its increases in complexity with:

`$  eksctl create cluster -f cluster.yaml`

Where `cluster.yaml` is:

    apiVersion: eksctl.io/v1alpha5
    kind: ClusterConfig

    metadata:
    name: basic-cluster
    region: eu-north-1

    nodeGroups:
    - name: ng-1
        instanceType: m5.large
        desiredCapacity: 10
        volumeSize: 50
        ssh:
            allow: true # will use ~/.ssh/id_rsa.pub as the default ssh key
    - name: ng-2
        instanceType: m5.xlarge
        desiredCapacity: 2
        volumeSize:100

These will handle all needed components to get it working and kubeconfig configured so you can issue `$ kubectl cluster-info` and `$ kubectl get nodes` in order to see your control plane and worker nodes on AWS.

# Deploying from Jenkins to K8s in AWS

## Install in Jenkins the following:

1. Install `Kubectl`  

2. Install `aws-iam-authenticator` tool

3. Create `KUBECONFIG` file to connect to EKS cluster, this works along with aws-iam-authenticator to get Jenkins connected to EKS cluster and AWS account.

4. Add AWS credentials on Jenkins for AWS account authentication.

5. Configure proper Jenkinsfile to deploy to EKS cluster.

## Enter as root in your Jenkins container

### KUBECTL

Install according to the [Documentation](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/)

Inside the Jenkins container:

`$ docker exec -u 0 -it {containerID} bash`

`$ curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"`

### AWS-IAM-AUTHENTICATOR

Install according to the [Documentation](https://docs.aws.amazon.com/eks/latest/userguide/install-aws-iam-authenticator.html)

Inside the Jenkins container:

`$ curl -o aws-iam-authenticator https://s3.us-west-2.amazonaws.com/amazon-eks/1.21.2/2021-07-05/bin/linux/amd64/aws-iam-authenticator`

`$ chmod +x ./aws-iam-authenticator`

Move it to a place that's already in your `$PATH` for ease.

`$ mv ./aws-iam-authenticator /usr/local/bin`

### Create KUBECONFIG FILE

[Offial Docs](https://docs.aws.amazon.com/eks/latest/userguide/create-kubeconfig.html)

You can create it by using aws cli or manually by using a bash script, also as you probably won't have any text editor inside the jenkins container create it somewhere else, in the docker host by example and then copy it inside.

#### Install aws cli inside Jenkins container 

`# curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"` 

`# unzip awscliv2.zip`

`# ./aws/install`

#### Set values for a few variables by replacing the example values with your own and then running the modified commands.

    export region_code=region-code
    export cluster_name=my-cluster
    export account_id=111122223333

#### Retrieve the endpoint for your cluster and store the value in a variable.

    cluster_endpoint=$(aws eks describe-cluster \
        --region $region_code \
        --name $cluster_name \
        --query "cluster.endpoint" \
        --output text)

#### Retrieve the Base64-encoded certificate data required to communicate with your cluster and store the value in a variable.

    certificate_data=$(aws eks describe-cluster \
        --region $region_code \
        --name $cluster_name \
        --query "cluster.certificateAuthority.data" \
        --output text)

#### Create the default `~/.kube` directory if it doesn't already exist.

    mkdir -p ~/.kube

#### Inside Jenkins container and having installed `aws cli` Run:

    #!/bin/bash
    read -r -d '' KUBECONFIG <<EOF
    apiVersion: v1
    clusters:
    - cluster:
        certificate-authority-data: $certificate_data
        server: $cluster_endpoint
    name: arn:aws:eks:$region_code:$account_id:cluster/$cluster_name
    contexts:
    - context:
        cluster: arn:aws:eks:$region_code:$account_id:cluster/$cluster_name
        user: arn:aws:eks:$region_code:$account_id:cluster/$cluster_name
    name: arn:aws:eks:$region_code:$account_id:cluster/$cluster_name
    current-context: arn:aws:eks:$region_code:$account_id:cluster/$cluster_name
    kind: Config
    preferences: {}
    users:
    - name: arn:aws:eks:$region_code:$account_id:cluster/$cluster_name
    user:
        exec:
        apiVersion: client.authentication.k8s.io/v1beta1
        command: aws
        args:
            - --region
            - $region_code
            - eks
            - get-token
            - --cluster-name
            - $cluster_name
            # - "- --role-arn"
            # - "arn:aws:iam::$account_id:role/my-role"
        # env:
            # - name: "AWS_PROFILE"
            #   value: "aws-profile"
    EOF
    echo "${KUBECONFIG}" > ~/.kube/config
#### Copy the ~/.kube/config generated to the container

Get jenkins containerID

`$ docker ps`

Enter

`docker exec -it {containerID} bash`

Create .kube dir in jenkins'home

`mkdir ~/.kube`

Copy kubeconfig file to it

`$ docker cp ~/.kube/config {containerID}:/var/jenkins_home/.kube/`

### Create a Jenkins IAM limited user on AWS as a best practice and define in Jenkins's credentials

![Jenkins's credentials](images/iam-user.png)

#### Configure a jenkinsfile like this by using the ID of the previously created credentials and using them as environment variables

### Make use of kubectl command to create/update deployment on connected K8s 


    #!/usr/bin/env groovy

    pipeline {
        agent any
        stages {
            stage('build app') {
                steps {
                script {
                    echo "building the application..."
                }
                }
            }
            stage('build image') {
                steps {
                    script {
                        echo "building the docker image..."
                    }
                }
            }
            stage('deploy') {
                environment {
                AWS_ACCESS_KEY_ID = credentials('jenkins_aws_access_key_id')
                AWS_SECRET_ACCESS_KEY = credentials('	jenkins_aws_secret_access_key')
                }
                steps {
                    script {
                    echo 'deploying docker image...'
                    sh 'kubectl create deployment nginx-deployment --image=nginx'
                    }
                }
            }
        }
    }

___

### CLEANUP!

Remember not to leave anything behind as AWS will charge you for many of the resources you've created.

`$ eksctl delete cluster -f cluster-demo.yaml`

![cleanup](images/cleanup.png)

Also look for other components created along this guide like roles or users.
____